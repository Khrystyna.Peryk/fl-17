// Your code goes here
const BirthdayService = require('../src/birthday.service');
describe('birthdayService: test suite 1', () => {
    it('checks if resolved', async () => {
        function checkIfResolved() {
            let myBdService = new BirthdayService();
            let myBd = new Date(1991, 1, 5);
            return myBdService.howLongToMyBirthday(myBd);
        }
        await checkIfResolved();
    });
});
describe('birthdayService: test suite 2', () => {
    it('checks if rejected', async () => {
        function checkIfRejected() {
            let myBdService = new BirthdayService();
            let myBd = 1991;
            return myBdService.howLongToMyBirthday(myBd);
        }
        try{
            await checkIfRejected();
        } catch(error) {
            return;
        }
        throw new Error('Promise should not be resolved');
    });
});