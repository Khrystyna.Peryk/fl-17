class StateStatus {
    constructor(name) {
        this.name = name;
    }
}
class ReadyForPushNotification extends StateStatus {
    constructor() {
        super('readyForPushNotification');
    } 
}
class ReadyForApprove extends StateStatus {
    constructor() {
        super('readyForApprove');
    }
}
class ReadyForPublish extends StateStatus {
    constructor() {
        super('readyForPublish');
    }
}
class PublishInProgress extends StateStatus {
    constructor() {
        super('publishInProgress');
    }
}
class Magazine {
    constructor() {
        this.states = [
            new ReadyForPushNotification(), 
            new ReadyForApprove(), 
            new ReadyForPublish(), 
            new PublishInProgress()
        ];
        this.state = this.states[0];
        this.employees = [];
        this.articles = [];
        this.followers = [];
    }
    nextState() {
        const totalStates = this.states.length;
        let currentIndex = this.states.findIndex(name => name === this.state);
        if (currentIndex + 1 < totalStates) {
            this.state = this.states[currentIndex + 1];
        } else {
            this.state = this.states[0];
        }
    }
    onUpdate(data) {
        data = this.articles;
        let resultsOfData = data.map(article => {
            let stringsWithFollowers = '';
            this.followers.map(follower => {
                let newfollower = follower.split(' : ');
                if (article.includes(newfollower[1])) {
                    stringsWithFollowers = stringsWithFollowers + `${article} ${newfollower[0]}`;
                }
                return stringsWithFollowers;
            });
            return stringsWithFollowers;          
        });
        resultsOfData.forEach(el => console.log(el));
    }
}

class MagazineEmployee {
    constructor (name, type, magazine) {
        this.name = name;
        this.type = type;
        this.magazine = magazine;
        this.magazine.employees.push(this.type);
    }
    approve() {
        if (this.type !== 'manager') {
            console.log('You do not have permissions to do it');
        } else {
            if (this.magazine.state.name === 'readyForPublish') {
                console.log(`Hello ${this.name}. Publications have been already approved by you.`);
            }
            if (this.magazine.state.name === 'readyForApprove') {
                this.magazine.nextState();
                console.log(`Hello ${this.name} You've approved the changes`);
            }
            if (this.magazine.state.name === 'readyForPushNotification') {
                console.log(`Hello ${this.name}.  You can't approve. We don't have enough of publications.`);
            }
            if (this.magazine.state.name === 'publishInProgress') {
                console.log(`Hello ${this.name}.  While we are publishing we can't do any actions`);
            }
        }
    }
    publish() {
        let magicMin = 60000;
        if (this.type === 'manager') {
            console.log('Only your employees have permissions to do it');
        } else {
            if (this.magazine.state.name === 'publishInProgress') {
                console.log(`Hello ${this.name}.  While we are publishing we can't do any actions`);
            }
            if (this.magazine.state.name === 'readyForApprove') {
                console.log(`Hello ${this.name} You can't publish. We don't have a manager's approval.`);
            }
            if (this.magazine.state.name === 'readyForPushNotification') {
                console.log(`Hello ${this.name}. You can't publish. We are creating publications now.`);
            }
            if (this.magazine.state.name === 'readyForPublish') {
                this.magazine.nextState();
                setTimeout(() => {
                    this.magazine.nextState();
                    this.magazine.onUpdate(this.magazine.articles);
                }, magicMin);
                console.log(`Hello ${this.name} You've recently published publications.`);
            }
        }
    }
    addArticle(article) {
        let magic5 = 5;
        this.magazine.articles.push(article);
        if (this.magazine.articles.length >= magic5 && this.magazine.state.name === 'readyForPushNotification') {
            this.magazine.nextState();
        }
    }
}
class Follower {
    constructor (name) {
        this.name = name;
    }
    subscribeTo(magazine, type) {
        magazine.followers.push(`${this.name} : ${type}`);
    }
    unsubscribeTo(magazine, type) {
        magazine.followers = magazine.followers.filter(sub => !sub.includes(`${this.name} : ${type}`));
    }
}