'use strict';

/**
 * Class
 * @constructor
 * @param size - size of pizza
 * @param type - type of pizza
 * @throws {PizzaException} - in case of improper use
 */

function Pizza(size, type) {
    let argumentsLength = 2;
    if (arguments.length !== argumentsLength) {
        throw new PizzaException(`Required two arguments, given: ${arguments.length}`);
    }
    this.size = size;
    this.type = type;
    if (!['SMALL', 'MEDIUM', 'LARGE'].includes(this.size)) {
        throw new PizzaException('Invalid size');
    } 
    if (!['VEGGIE', 'MARGHERITA', 'PEPPERONI'].includes(this.type)) {
        throw new PizzaException('Invalid type');
    }
    let ingArray = [];
    this.addExtraIngredient = function(ingredient) {
        if (arguments.length > 1) {
            throw new PizzaException(`Required one arguments, given: ${arguments.length}`);
        }
        if (!['TOMATOES', 'CHEESE', 'MEAT'].includes(ingredient)) {
            throw new PizzaException('Invalid ingredient');
        }
        if (ingArray.includes(ingredient)) {
            throw new PizzaException('Duplicate ingredient');
        } else {
            ingArray.push(ingredient);
        }
    };
    this.removeExtraIngredient = function(ingredient) {
        if (arguments.length > 1) {
            throw new PizzaException(`Required one arguments, given: ${arguments.length}`);
        }
        if (!ingArray.includes(ingredient)) {
            throw new PizzaException(`There is no ${ingredient} in your pizza`);
        } else {
            for (let i = 0; i < ingArray.length; i++) {
                if (ingArray[i] === ingredient) {
                    ingArray.splice(i, 1);
                }
            }
        }
    };
    this.getSize = function() {
        return `${this.size}`;
    };
    let price = Pizza.allowedSizes[`${this.size}`] + Pizza.allowedTypes[`${this.type}`];
    this.getPrice = function() {
        if (ingArray.length === 0) {
            return `Price: ${price} UAH`;
        } else {
            let newArr = ingArray.map( x => {
                for (const [key, value] of Object.entries(Pizza.allowedExtraIngredients)) {
                    if (x === key) {
                        x = value;
                    }
                }
                return x;
            });
            price = Pizza.allowedSizes[`${this.size}`] + Pizza.allowedTypes[`${this.type}`] + 
            newArr.reduce((a,b) => a + b, 0);
            return `Price with extra ingredients: ${price} UAH`;
        }
    };
    this.getExtraIngredients = function() {
        return ingArray.join();
    };
    this.getPizzaInfo = function() {
        if (ingArray.length === 0) {
            return `Size: ${this.size}, type: ${this.type}; extra ingredients: ${ingArray.join()}; price: ${price} UAH`;
        } else {
            let newArr = ingArray.map( x => {
                for (const [key, value] of Object.entries(Pizza.allowedExtraIngredients)) {
                    if (x === key) {
                        x = value;
                    }
                }
                return x;
            });
            price = Pizza.allowedSizes[`${this.size}`] + Pizza.allowedTypes[`${this.type}`] + 
            newArr.reduce((a,b) => a + b, 0);
            return `Size: ${this.size}, type: ${this.type}; extra ingredients: ${ingArray.join()}; price: ${price} UAH`;
        }
    };
 }



/* Sizes, types and extra ingredients */

Pizza.SIZE_S = 'SMALL';
Pizza.SIZE_M = 'MEDIUM';
Pizza.SIZE_L = 'LARGE';

Pizza.TYPE_VEGGIE = 'VEGGIE';
Pizza.TYPE_MARGHERITA = 'MARGHERITA';
Pizza.TYPE_PEPPERONI = 'PEPPERONI';

Pizza.EXTRA_TOMATOES = 'TOMATOES';
Pizza.EXTRA_CHEESE = 'CHEESE';
Pizza.EXTRA_MEAT = 'MEAT';

/* Allowed properties */
Pizza.allowedSizes = {'SMALL' : 50, 'MEDIUM' : 75, 'LARGE' : 100};
Pizza.allowedTypes = {'VEGGIE' : 50, 'MARGHERITA' : 60, 'PEPPERONI' : 70};
Pizza.allowedExtraIngredients = {'TOMATOES' : 5, 'CHEESE' : 7, 'MEAT' : 9};


/**
 * Provides information about an error while working with a pizza.
 * details are stored in the log property.
 * @constructor
 */
function PizzaException(log) {
    this.log = log;
}


/* It should work */ 
// // small pizza, type: veggie
// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// // add extra meat
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// // check price
// console.log(`Price: ${pizza.getPrice()} UAH`); //=> Price: 109 UAH
// // add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_CHEESE);
// // add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_TOMATOES);
// // check price
// console.log(`Price with extra ingredients: ${pizza.getPrice()} UAH`); // Price: 121 UAH
// // check pizza size
// console.log(`Is pizza large: ${pizza.getSize() === Pizza.SIZE_L}`); //=> Is pizza large: false
// // remove extra ingredient
// pizza.removeExtraIngredient(Pizza.EXTRA_CHEESE);
// console.log(`Extra ingredients: ${pizza.getExtraIngredients().length}`); //=> Extra ingredients: 2
// console.log(pizza.getPizzaInfo()); //=> Size: SMALL, type: VEGGIE; extra ingredients: MEAT,TOMATOES; price: 114UAH.

// examples of errors
// let pizza = new Pizza(Pizza.SIZE_S); // => Required two arguments, given: 1

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.SIZE_S); // => Invalid type

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT); // => Duplicate ingredient

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT); // => Invalid ingredient
