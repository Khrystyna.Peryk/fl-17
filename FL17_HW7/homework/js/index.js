let minusOne = -1;
let twoVar = 2;
function insertCharacter(char) {
	if ($('.inputDisplay').val() === '0') {
		$('.inputDisplay').val('');
	}
	$('.inputDisplay').val($('.inputDisplay').val() + char).css('color', 'black');
	let currentValue = $('.inputDisplay').val();
	let lastChar = currentValue[currentValue.length-1];


	if (/(^-?\d+[-+/*]\d+)/g.test(currentValue) && lastChar.includes('-')) {
		let currValWithoutLast = currentValue.slice(0, minusOne);
		$('.inputDisplay').val(currValWithoutLast);
		for (let i = currValWithoutLast.length - 1; i >= 0; i--) {
			if (isNaN(currValWithoutLast[i] - 1)) {
				let replacedVal = currentValue.replace(currValWithoutLast[i], lastChar);
				$('.inputDisplay').val(replacedVal.slice(0, minusOne));
				break;
			}
		}
	}
	if (/(^-?\d+[-+/*]\d+)/g.test(currentValue) && lastChar.includes('+')) {
		let currValWithoutLast = currentValue.slice(0, minusOne);
		$('.inputDisplay').val(currValWithoutLast);
		for (let i = currValWithoutLast.length - 1; i >= 0; i--) {
			if (isNaN(currValWithoutLast[i] - 1)) {
				let replacedVal = currentValue.replace(currValWithoutLast[i], lastChar);
				$('.inputDisplay').val(replacedVal.slice(0, minusOne));
				break;
			}
		}
	}
	if (/(^-?\d+[-+/*]\d+)/g.test(currentValue) && lastChar.includes('*')) {
		let currValWithoutLast = currentValue.slice(0, minusOne);
		$('.inputDisplay').val(currValWithoutLast);
		for (let i = currValWithoutLast.length - 1; i >= 0; i--) {
			if (isNaN(currValWithoutLast[i] - 1)) {
				let replacedVal = currentValue.replace(currValWithoutLast[i], lastChar);
				$('.inputDisplay').val(replacedVal.slice(0, minusOne));
				break;
			}
		}
	}
	if (/(^-?\d+[-+/*]\d+)/g.test(currentValue) && lastChar.includes('/')) {
		let currValWithoutLast = currentValue.slice(0, minusOne);
		$('.inputDisplay').val(currValWithoutLast);
		for (let i = currValWithoutLast.length - 1; i >= 0; i--) {
			if (isNaN(currValWithoutLast[i] - 1)) {
				let replacedVal = currentValue.replace(currValWithoutLast[i], lastChar);
				$('.inputDisplay').val(replacedVal.slice(0, minusOne));
				break;
			}
		}
	}
}
function result() {
	let currentValue = $('.inputDisplay').val();
	let arrayOfCurrVals = [];
	let nanCount = 0;
	for (let i = currentValue.length - 1; i >= 0; i--) {
		if (isNaN(currentValue[i] - 1)) {
			nanCount++;
		}
	}
	if (nanCount > twoVar) {
		$('.inputDisplay').val('ERROR, no more than two nums').css('color', 'red');
	}
	if (nanCount <= twoVar && currentValue.startsWith('/')) {
		$('.inputDisplay').val('ERROR').css('color', 'red');
	} else if (nanCount <= twoVar && currentValue.startsWith('*')) {
		$('.inputDisplay').val('ERROR').css('color', 'red');
	} else if (nanCount <= twoVar && currentValue.includes('/0')) {
		$('.inputDisplay').val('ERROR').css('color', 'red');
	} else if (nanCount <= twoVar && !currentValue.includes('/0')) {
		for (let i = currentValue.length - 1; i >= 0; i--) {
			if (isNaN(currentValue[i] - 1)) {
				arrayOfCurrVals = currentValue.split(currentValue[i]);
				if (arrayOfCurrVals[0] === '' && currentValue[i] === '-') {
					arrayOfCurrVals.shift(arrayOfCurrVals[0]);
					arrayOfCurrVals[0] = `-${arrayOfCurrVals[0]}`;
					arrayOfCurrVals[1] = `-${arrayOfCurrVals[1]}`;
					arrayOfCurrVals[twoVar] = '+';
				} else if (arrayOfCurrVals[0] === '' && currentValue[i] === '+') {
					arrayOfCurrVals.shift(arrayOfCurrVals[0]);
					arrayOfCurrVals[0] = `+${arrayOfCurrVals[0]}`;
					arrayOfCurrVals[1] = `+${arrayOfCurrVals[1]}`;
				}
				arrayOfCurrVals.push(currentValue[i]);
				let [a, b, operator] = arrayOfCurrVals;
				if (operator === '+') {
					$('.logs-wrapper')
					.prepend(`<p><span class="circle"></span>${Number(a)} + ${Number(b)} = ${Number(a) + Number(b)}
					<span class="cross">&#215;</span></p>`);
					$('p').addClass('logsValue');
					return $('.inputDisplay').val(`${Number(a) + Number(b)}`);
				} else if (operator === '-') {
					$('.logs-wrapper')
					.prepend(`<p><span class="circle"></span>${Number(a)} - ${Number(b)} = ${Number(a) - Number(b)}
					<span class="cross">&#215;</span></p>`);
					$('p').addClass('logsValue');
					return $('.inputDisplay').val(`${Number(a) - Number(b)}`);
				} else if (operator === '*') {
					$('.logs-wrapper')
					.prepend(`<p><span class="circle"></span>${Number(a)} * ${Number(b)} = ${Number(a) * Number(b)}
					<span class="cross">&#215;</span></p>`);
					$('p').addClass('logsValue');
					return $('.inputDisplay').val(`${Number(a) * Number(b)}`);
				} else if (operator === '/') {
					$('.logs-wrapper')
					.prepend(`<p><span class="circle"></span>${Number(a)} / ${Number(b)} = ${Number(a) / Number(b)}
					<span class="cross">&#215;</span></p>`);
					$('p').addClass('logsValue');
					return $('.inputDisplay').val(`${Number(a) / Number(b)}`);
				}
			}
		}
	}
}
function clearInput() {
	$('.inputDisplay').val('');
}

$(document).on('mouseover', '.cross', function() {
	$(this).css('color', 'red');
});
$(document).on('mouseover', '.circle', function() {
	$(this).css('background-color', 'red');
});
$(document).on('click', '.cross', function() {
	$(this).parent().hide();
});
$(document).on('click', '.circle', function() {
	$(this).toggleClass('red');
});
$(document).ready(function() {
	$('.logs-wrapper').scroll(function() {
		console.log(`Scroll Top: ${$('.logs-wrapper').scrollTop()}`);
	});
});