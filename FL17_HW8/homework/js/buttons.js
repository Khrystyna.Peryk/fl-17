const rockBtn = document.querySelector(".rock");
const paperBtn = document.querySelector(".paper");
const scissorBtn = document.querySelector(".scissor");
export const playerOptions = [rockBtn, paperBtn, scissorBtn];
export const computerOptions = ["rock", "paper", "scissors"];
