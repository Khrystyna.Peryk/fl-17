export const alertMessageDiv = document.getElementById('alertMessage');
export const alertMessageTextSpan = document.getElementById('alertMessageText');
export const tweetItemsMainPage = document.getElementById('tweetItems');
export const modifyItem = document.getElementById('modifyItem');
export const h1 = document.getElementById('modifyItemHeader');
export const listOfTweets = document.getElementById('list');
export const navigationButtonsDiv = document.getElementById('navigationButtons');