// Your code goes here
function getBiggestNumber(...arg) {
    let twoVar = 2;
    let tenVar = 10;
    let isNumber = [...arg].every( el => {
        return typeof el === 'number';
    });
    if (isNumber === false) {
        throw new Error('Wrong argument type');
    } else if (!arguments || arguments.length < twoVar) {
        throw new Error('Not enough arguments');
    } else if (arguments.length > tenVar) {
        throw new Error('Too many arguments');
    }
    return Math.max(...arg);
}
module.exports = getBiggestNumber;