import { playerOptions, computerOptions } from "./buttons";
import {
  var3,
  movesLeft,
  result,
  playerScoreBoard,
  computerScoreBoard,
} from "./options";
import "../scss/style1.scss";
const game = () => {
  let playerScore = 0;
  let computerScore = 0;
  let moves = 0;
  const playGame = () => {
    playerOptions.forEach((option) => {
      option.addEventListener("click", function () {
        moves++;
        const choiceNumber = Math.floor(Math.random() * var3);
        const computerChoice = computerOptions[choiceNumber];
        winner(this.innerText, computerChoice);
        if (moves === var3) {
          gameOver(playerOptions, movesLeft);
        }
      });
    });
  };
  const winner = (player, computer) => {
    player = player.toLowerCase();
    computer = computer.toLowerCase();
    if (player === computer) {
      result.textContent = `Round ${moves} - Tie`;
    } else if (player === "rock") {
      if (computer === "paper") {
        result.textContent = `Round ${moves} Rock vs Paper - You have LOST!`;
        computerScore++;
        computerScoreBoard.textContent = computerScore;
      } else {
        result.textContent = `Round ${moves} Rock vs Scissors - You have WON!`;
        playerScore++;
        playerScoreBoard.textContent = playerScore;
      }
    } else if (player === "scissors") {
      if (computer === "rock") {
        result.textContent = `Round ${moves} Scissors vs Rock - You have LOST!`;
        computerScore++;
        computerScoreBoard.textContent = computerScore;
      } else {
        result.textContent = `Round ${moves} Scissors vs Paper - You have WON!`;
        playerScore++;
        playerScoreBoard.textContent = playerScore;
      }
    } else if (player === "paper") {
      if (computer === "scissors") {
        result.textContent = `Round ${moves} Paper vs Scissors - You have LOST!`;
        computerScore++;
        computerScoreBoard.textContent = computerScore;
      } else {
        result.textContent = `Round ${moves} Paper vs Rock - You have WON!`;
        playerScore++;
        playerScoreBoard.textContent = playerScore;
      }
    }
  };
  const gameOver = (playerOptions) => {
    const chooseMove = document.querySelector(".move");
    const result = document.querySelector(".result");
    const reloadBtn = document.querySelector(".reset");
    playerOptions.forEach((option) => {
      option.style.display = "none";
    });
    chooseMove.innerText = "Game Over!!";
    if (playerScore > computerScore) {
      result.innerText = "You Won The Game";
      result.style.color = "#308D46";
    } else if (playerScore < computerScore) {
      result.innerText = "You Lost The Game";
      result.style.color = "red";
    } else {
      result.innerText = "Tie";
      result.style.color = "grey";
    }
    reloadBtn.innerText = "-- RESET --";
    reloadBtn.addEventListener("click", () => {
      window.location.reload();
    });
  };
  playGame();
};
game();
