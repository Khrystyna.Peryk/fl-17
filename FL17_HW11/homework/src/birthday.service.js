// Your code goes here
class BirthdayService {
    howLongToMyBirthday(date) {
        let today = new Date();
        const daysLeft = (date, today) => {
            return new Promise((resolve, reject) => {
                let hundred = 100;
                if (!(date instanceof Date && !isNaN(date))) {
                    reject('Wrong argument');
                } else {
                    setTimeout(() => {
                        if (date.getDate() === today.getDate() && 
                        date.getMonth() === today.getMonth()) {
                            resolve(this.congratulateWithBirthday());
                        } else {
                            date.setFullYear(today.getFullYear());
                            if (today > date) {
                            date.setFullYear(today.getFullYear() + 1);
                            }
                            let thousand = 1000;
                            let sixty = 60;
                            let twentyFour = 24;
                            let result = Math.floor((date - today) / (thousand*sixty*sixty*twentyFour));
                            resolve(this.notifyWaitingTime(result));
                        }
                    }, hundred);
                }
            });
        };
        return daysLeft(date, today);
    }
    congratulateWithBirthday() {
        console.log('Hooray!!! It is today!');
    }
    notifyWaitingTime(waitingTime) {
        let halfYear = 182;
        let fullYear = 365;
        if (waitingTime < halfYear) {
            console.log(`Soon...Please, wait just ${waitingTime} day/days`);
        } else {
            console.log(`Oh, you have celebrated it ${fullYear - waitingTime} day/s ago, don't you remember?`);
        }
    }
}
module.exports = BirthdayService;