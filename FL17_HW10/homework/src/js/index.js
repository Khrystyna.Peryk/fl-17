import {alertMessageDiv,
    alertMessageTextSpan,
    tweetItemsMainPage,
    modifyItem,
    h1,
    listOfTweets,
    navigationButtonsDiv
} from './selectedElements1';
import {textarea,
    addTweetButton,
    saveModifiedItemButton,
    cancelModificationButton,
    seconds2,
    mainPage,
    h1s
} from './selectedElements2';
import '../scss/styles.scss';

window.onpopstate = function() {
    if (location.hash === '#/add') {
        modifyItem.style.display = 'flex';
        tweetItemsMainPage.style.display = 'none';
        h1.innerText = 'Add tweet';
    } else if (location.hash.startsWith('#/edit')) {
        modifyItem.style.display = 'flex';
        tweetItemsMainPage.style.display = 'none';
        h1.innerText = 'Edit tweet';
    } else if (location.hash === '#/liked') {
        modifyItem.style.display = 'none';
        tweetItemsMainPage.style.display = 'flex';
        h1s[0].innerText = 'Liked tweets';
        addTweetButton[0].innerText = 'back';
    } else {
        modifyItem.style.display = 'none';
        tweetItemsMainPage.style.display = 'flex';
    }
};
addTweetButton[0].addEventListener('click', () => {
    location.hash = '#/add';
});
for (let key of Object.keys(localStorage)) {
    let newLi = document.createElement('li');
    listOfTweets.appendChild(newLi);
    newLi.innerText = key;
    let spanBtn = document.createElement('span');
    spanBtn.setAttribute('class', 'spanBtn');
    newLi.appendChild(spanBtn);
    let removeButton = document.createElement('button');
    removeButton.setAttribute('class', 'removes');
    let likeButton = document.createElement('button');
    likeButton.setAttribute('class', 'likeBtn');
    spanBtn.appendChild(removeButton);
    removeButton.innerText = 'remove';
    spanBtn.appendChild(likeButton);
    likeButton.innerText = 'like';
}
cancelModificationButton.addEventListener('click', () => {
    location = mainPage; 
});
saveModifiedItemButton.addEventListener('click', () => {
    if (Object.keys(localStorage).includes(textarea.value)) {
        alertMessageDiv.classList.toggle('grey');
        alertMessageTextSpan.innerText = "Error! You can't tweet about that";
        setTimeout(function() {
            alertMessageDiv.classList.toggle('grey');
            alertMessageTextSpan.innerText = ' ';
        }, seconds2); 
    } else if (!textarea.value) {
        console.log('need value');
    } else {
        localStorage.setItem(textarea.value, 'tweet');
        location = mainPage;
    }
});
listOfTweets.addEventListener('click', (e) => {
    let objWithNums = Object.assign({}, Object.keys(localStorage));
    let elevenVar = -11;
    let tenVar = -10;
    if (Object.values(objWithNums).includes(e.target.innerText.slice(0, elevenVar))) {
        let valueNumOfobjWithNums = Object.keys(objWithNums).find(key => 
            objWithNums[key] === e.target.innerText.slice(0, elevenVar));    
        location.hash = `#/edit:item_${valueNumOfobjWithNums}`;
        textarea.value = e.target.innerText.slice(0, tenVar);
        localStorage.removeItem(e.target.innerText.slice(0, tenVar));
        modifyItem.style.display = 'flex';
        tweetItemsMainPage.style.display = 'none';
        h1.innerText = 'Edit tweet';
    }
});
let removeBtns = document.getElementsByClassName('removes');
for (let i = 0; i < removeBtns.length; i++) {
    removeBtns[i].addEventListener('click', (e) => { 
        e.target.parentElement.parentElement.remove();
        let minusTenVar = -10;
        localStorage.removeItem(e.target.parentElement.parentElement.innerText.slice(0, minusTenVar));
    });
}
let goToLikedButton = document.createElement('button');
goToLikedButton.innerText = 'Go to Liked';
goToLikedButton.style.display = 'none';
navigationButtonsDiv.appendChild(goToLikedButton);
let likedPageItems = document.getElementsByClassName('likedPageItems');
let likeBtns = document.getElementsByClassName('likeBtn');
for (let i = 0; i < likeBtns.length; i++) {
    likeBtns[i].addEventListener('click', (e) => {
        if (e.target.innerText === 'like') {
            e.target.innerText = 'unlike';
            goToLikedButton.style.display = 'inline';
            e.target.parentElement.parentElement.setAttribute('class', 'likedPageItems');
            alertMessageDiv.classList.toggle('grey');
            alertMessageTextSpan.innerText = `Hooray! You liked tweet with id ${i}!`;
            setTimeout(() => {
                alertMessageDiv.classList.toggle('grey');
                alertMessageTextSpan.innerText = ' ';
            }, seconds2);
        } else {
            e.target.innerText = 'like';
            e.target.parentElement.parentElement.removeAttribute('class');
            if (likedPageItems.length === 0) {
                goToLikedButton.style.display = 'none';
            } else {
                goToLikedButton.style.display = 'inline';
            }
            alertMessageDiv.classList.toggle('grey');
            alertMessageTextSpan.innerText = `Sorry you no longer like tweet with id ${i}`;
            setTimeout(() => {
                alertMessageDiv.classList.toggle('grey');
                alertMessageTextSpan.innerText = ' ';
            }, seconds2);
        }
    });
}
goToLikedButton.addEventListener('click', () => {
    location.hash = '#/liked';
    goToLikedButton.style.display = 'none';
    addTweetButton.innerText = 'back';
    for (let li of listOfTweets.children) {
        if (li.getAttribute('class') === null) {
            li.style.display = 'none';
        }
    }
});