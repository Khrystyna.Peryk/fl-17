async function dataRendering() {
    await fetch('https://jsonplaceholder.typicode.com/users')
    .then(response => response.json())
    .then(users => displayList(users))
    .catch(error => console.log(error));
    editData();
    deleteData();
}
dataRendering();
function displayList(usersArray) {
    for (let user = 0; user < usersArray.length; user++) {
        let li = document.createElement('li');
        let ul = document.querySelector('ul');
        ul.appendChild(li);
        let spanId = document.createElement('span');
        li.append(spanId);
        spanId.append('id: ', `${usersArray[user].id}`);
        let spanName = document.createElement('span');
        li.append(spanName);
        spanName.append('name: ', `${usersArray[user].name}`);
        let spanUsername = document.createElement('span');
        li.append(spanUsername);
        spanUsername.append('username: ', `${usersArray[user].username}`);
        let spanEmail = document.createElement('span');
        li.append(spanEmail);
        spanEmail.append('email: ', `${usersArray[user].email}`);
        let spanPhone = document.createElement('span');
        li.append(spanPhone);
        spanPhone.append('phone: ', `${usersArray[user].phone}`);
        let spanWebsite = document.createElement('span');
        li.append(spanWebsite);
        spanWebsite.append('website: ', `${usersArray[user].website}`);
        let spanAddressCity = document.createElement('span');
        li.append('Address: ', spanAddressCity);
        spanAddressCity.append('city: ', `${usersArray[user].address.city}`);
        let spanAddressStreet = document.createElement('span');
        li.append(spanAddressStreet);
        spanAddressStreet.append('street: ', `${usersArray[user].address.street}`);
        let spanAddressSuite = document.createElement('span');
        li.append(spanAddressSuite);
        spanAddressSuite.append('suite: ', `${usersArray[user].address.suite}`);
        let spanAddressZipcode = document.createElement('span');
        li.append(spanAddressZipcode);
        spanAddressZipcode.append('zipcode: ', `${usersArray[user].address.zipcode}`);
        let spanAddressGeoLat = document.createElement('span');
        li.append('Geo: ', spanAddressGeoLat);
        spanAddressGeoLat.append('lat: ', `${usersArray[user].address.geo.lat}`);
        let spanAddressGeoLng = document.createElement('span');
        li.append(spanAddressGeoLng);
        spanAddressGeoLng.append('lng: ', `${usersArray[user].address.geo.lng}`);
        let spanAddressCompanyName = document.createElement('span');
        li.append('Company: ', spanAddressCompanyName);
        spanAddressCompanyName.append('name: ', `${usersArray[user].company.name}`);
        let spanAddressCompanyBs = document.createElement('span');
        li.append(spanAddressCompanyBs);
        spanAddressCompanyBs.append('bs: ', `${usersArray[user].company.bs}`);
        let spanAddressCompanyPhrase = document.createElement('span');
        li.append(spanAddressCompanyPhrase);
        spanAddressCompanyPhrase.append('catchPhrase: ', `${usersArray[user].company.catchPhrase}`);
        let button = document.createElement('button');
        li.append(button);
        button.append(`delete user`);
    }
}
function editData() {
    let spanArray = document.getElementsByTagName('span');
    for (let i = 0; i < spanArray.length; i++) {
        let span = spanArray[i];
        span.addEventListener('click', (e) => {
        let newData = prompt('Edit your data here:', e.target.innerHTML);
        let userId = e.target.parentElement.children[0].innerHTML;
        let fourVar = 4;
        userId = userId.substring(fourVar);
            if (newData && newData !== e.target.innerHTML) {
                e.target.innerHTML = newData;
                let properties = newData.split(', ');
                let obj = {};
                properties.forEach(function(property) {
                    let tup = property.split(':');
                    obj[tup[0]] = tup[1].trim();
                });
                showSpinner();
                fetch(`https://jsonplaceholder.typicode.com/users/${userId}`, {
                    method: 'PUT',
                    body: JSON.stringify(
                        obj
                    ),
                    headers: {
                    'Content-type': 'application/json; charset=UTF-8'
                    }
                })
                .then((response) => console.log(response.json()))
                .then((json) => console.log(json))
                .catch(error => console.log(error));
            }
        });
    }
}
function deleteData() {
    let buttonsArray = document.getElementsByTagName('button');
    for (let i = 0; i < buttonsArray.length; i++) {
        let button = buttonsArray[i];
        button.addEventListener('click', (e) => {
            e.stopPropagation();
            let userId = e.target.parentElement.children[0].innerHTML;
            let fourVar = 4;
            userId = userId.substring(fourVar);
            e.target.parentElement.remove();
            showSpinner();
            fetch(`https://jsonplaceholder.typicode.com/users/${userId}`, {
                method: 'DELETE'
            });  
        });
    }
}
const spinner = document.getElementById('spinner');
function showSpinner() {
  spinner.className = 'show';
  let fiveHundVar = 500;
  setTimeout(() => {
    spinner.className = spinner.className.replace('show', '');
  }, fiveHundVar);
}
