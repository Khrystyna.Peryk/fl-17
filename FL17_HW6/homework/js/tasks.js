let getMaxEvenElement = arr => {
    let newArr = [];
    let var2 = 2;
    let singleStr = arr.reduce((sum, item) => sum + item);
    for (let i of singleStr) {
        if (+i % var2 === 0) {
            newArr.push(i);
        }
    }
    return Math.max(...newArr);
}
let a = 3;
let b = 5;
[b, a] = [a, b];
let getValue = val => val ?? '-';
let getObjFromArray = arrayOfArrays => {
    let obj = {};
    for (let arr of arrayOfArrays) {
        obj[`${arr[0]}`] = arr[1];
    }
    return obj;
}
let addUniqueId = obj => {
    let newObj = {...obj, id: Symbol()};
    return newObj;
}
let getRegroupedObject = oldObj => {
    let {
        name,
        details: {
            id,
            age,
            university
        }
    } = oldObj;
    return {
        university,
        user: {
            age,
            firstname: name,
            id
        }
    };
}
let getArrayWithUniqueElements = arr => [... new Set(arr)];
let hideNumber = phoneNumber => {
    const Minus4 = -4;
    return phoneNumber.slice(Minus4).padStart(phoneNumber.length, '*');
}
let requiredA = () => {
    throw new Error('a is required');
}    
let requiredB = () => { 
    throw new Error('b is required');
}   
let add = (a = requiredA(), b = requiredB()) => a + b;
function* generateIterableSequence() {
    yield 'I';
    yield 'love';
    yield 'EPAM';
}