// Your code goes here
const getBiggestNumber = require('../src/get-bigest-number');
describe('getBiggestNumber: test suite 1', () => {
    it('returns biggest number', async () => {
        const result = getBiggestNumber(23, 9);
        expect(result).toBe(23);
    });
});
describe('getBiggestNumber: test suite 2', () => {
    it('checks if enough args', async () => {
        function checkIfEnoughArgs() {
            getBiggestNumber(1);
        }
        expect(checkIfEnoughArgs).toThrowError(Error, 'Not enough arguments');
    });
});
describe('getBiggestNumber: test suite 3', () => {
    it('checks if too many args', async () => {
        function checkIfTooManyArgs() {
            getBiggestNumber(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
        }
        expect(checkIfTooManyArgs).toThrowError(Error, 'Too many arguments');
    });
});
describe('getBiggestNumber: test suite 4', () => {
    it('checks arg type', async () => {
        function checkArgType() {
            getBiggestNumber('string');
        }
        expect(checkArgType).toThrowError(Error, 'Wrong argument type');
    });
});