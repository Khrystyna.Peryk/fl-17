function getProperTime() {
    let time = new Date();
    let properTime = time.toLocaleString('en-US', {
        hour: 'numeric', minute: 'numeric', second: 'numeric', hour12: true 
    });
    return properTime;
}
window.onload = function() {
    let userName = prompt('Enter your name please:');
    let form = document.getElementById('message-form');
    let messageField = document.getElementById('message');
    let messagesList = document.getElementById('messages');
    let socketStatus = document.getElementById('status');
    let closeBtn = document.getElementById('close');
    let socket = new WebSocket('ws://localhost:8080');
    socket.onopen = function(event) {
        socketStatus.innerHTML = 'Connected to: ' + event.currentTarget.url;
        socketStatus.className = 'open';
      };
    socket.onerror = function(error) {
        console.log('WebSocket Error: ' + error);
    };
    form.onsubmit = function(e) {
        e.preventDefault();
        let message = messageField.value;
        socket.send(JSON.stringify({
            'userName' : userName,
            'message' : message
        }));
        messagesList.innerHTML += `<li class='sent'><div class='username'>${userName}:</div>` + 
                                message + `<div class='time'>${getProperTime()}<div></li>`;
        messageField.value = '';
        return false;
    };
    socket.onmessage = function(event) {
        let messageSent = JSON.parse(event.data);
        console.dir(event);
        messagesList.innerHTML += `<li class='received'><div class='username'>${messageSent.userName}:</div>` +
                                messageSent.message + `<div class='time'>${getProperTime()}<div></li>`;
    };
    socket.onclose = function() {
        socketStatus.innerHTML = 'Disconnected from WebSocket.';
        socketStatus.className = 'closed';
    };
    closeBtn.onclick = function(e) {
        e.preventDefault();
        socket.close();
        return false;
    };
};